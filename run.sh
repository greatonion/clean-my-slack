#!/usr/bin/env bash

#docker build -t slack-cleaner .

token="xoxp-132251707268-fake"

echo "git"
docker run -ti tdatest/slack-cleaner slack-cleaner --token $token  --message --channel git --bot --perform

echo "general"
docker run -ti tdatest/slack-cleaner slack-cleaner --token $token  --message --channel general --user "*" --perform

echo "random"
docker run -ti tdatest/slack-cleaner slack-cleaner --token $token  --message --channel random --user "*" --perform

echo "td + adam"
docker run -ti tdatest/slack-cleaner slack-cleaner --token $token  --message --direct adam --user tomasz --perform

echo "td + kg"
docker run -ti tdatest/slack-cleaner slack-cleaner --token $token  --message --direct krzysztof --user tomasz --perform

echo "td + ls"
docker run -ti tdatest/slack-cleaner slack-cleaner --token $token  --message --direct laco --user tomasz --perform

